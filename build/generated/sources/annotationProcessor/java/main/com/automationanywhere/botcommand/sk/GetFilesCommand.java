package com.automationanywhere.botcommand.sk;

import com.automationanywhere.bot.service.GlobalSessionContext;
import com.automationanywhere.botcommand.BotCommand;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import java.lang.Boolean;
import java.lang.ClassCastException;
import java.lang.Deprecated;
import java.lang.Object;
import java.lang.String;
import java.lang.Throwable;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class GetFilesCommand implements BotCommand {
  private static final Logger logger = LogManager.getLogger(GetFilesCommand.class);

  private static final Messages MESSAGES_GENERIC = MessagesFactory.getMessages("com.automationanywhere.commandsdk.generic.messages");

  @Deprecated
  public Optional<Value> execute(Map<String, Value> parameters, Map<String, Object> sessionMap) {
    return execute(null, parameters, sessionMap);
  }

  public Optional<Value> execute(GlobalSessionContext globalSessionContext,
      Map<String, Value> parameters, Map<String, Object> sessionMap) {
    logger.traceEntry(() -> parameters != null ? parameters.toString() : null, ()-> sessionMap != null ?sessionMap.toString() : null);
    GetFiles command = new GetFiles();
    HashMap<String, Object> convertedParameters = new HashMap<String, Object>();
    if(parameters.containsKey("sessionName") && parameters.get("sessionName") != null && parameters.get("sessionName").get() != null) {
      convertedParameters.put("sessionName", parameters.get("sessionName").get());
      if(!(convertedParameters.get("sessionName") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","sessionName", "String", parameters.get("sessionName").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("sessionName") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","sessionName"));
    }

    if(parameters.containsKey("li_name") && parameters.get("li_name") != null && parameters.get("li_name").get() != null) {
      convertedParameters.put("li_name", parameters.get("li_name").get());
      if(!(convertedParameters.get("li_name") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","li_name", "String", parameters.get("li_name").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("li_name") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","li_name"));
    }

    if(parameters.containsKey("doctype") && parameters.get("doctype") != null && parameters.get("doctype").get() != null) {
      convertedParameters.put("doctype", parameters.get("doctype").get());
      if(!(convertedParameters.get("doctype") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","doctype", "String", parameters.get("doctype").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("doctype") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","doctype"));
    }
    if(convertedParameters.get("doctype") != null) {
      switch((String)convertedParameters.get("doctype")) {
        case "SUCCESS" : {

        } break;
        case "INVALID" : {

        } break;
        case "UNCLASSIFIED" : {

        } break;
        case "UNTRAINED" : {

        } break;
        case "VALIDATION" : {

        } break;
        default : throw new BotCommandException(MESSAGES_GENERIC.getString("generic.InvalidOption","doctype"));
      }
    }

    if(parameters.containsKey("getURL") && parameters.get("getURL") != null && parameters.get("getURL").get() != null) {
      convertedParameters.put("getURL", parameters.get("getURL").get());
      if(!(convertedParameters.get("getURL") instanceof Boolean)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","getURL", "Boolean", parameters.get("getURL").get().getClass().getSimpleName()));
      }
    }

    command.setSessions(sessionMap);
    try {
      Optional<Value> result =  Optional.ofNullable(command.action((String)convertedParameters.get("sessionName"),(String)convertedParameters.get("li_name"),(String)convertedParameters.get("doctype"),(Boolean)convertedParameters.get("getURL")));
      return logger.traceExit(result);
    }
    catch (ClassCastException e) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.IllegalParameters","action"));
    }
    catch (BotCommandException e) {
      logger.fatal(e.getMessage(),e);
      throw e;
    }
    catch (Throwable e) {
      logger.fatal(e.getMessage(),e);
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.NotBotCommandException",e.getMessage()),e);
    }
  }
}
